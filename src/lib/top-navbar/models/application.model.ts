export interface Application {
  app_id: Number;
  title: String;
  url: String;
  description?: String;
  data_source?: String;
  stage?: String;
  months_used?: String;
  verified?: Boolean;
  maintainer?: String;
  active?: Boolean;
  crowd_id?: String;
  permission?: String;
}