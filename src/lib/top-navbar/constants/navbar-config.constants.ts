import { InjectionToken } from '@angular/core';
import { NavbarConfig } from '../models/navbar-config.model';

export const NAVBAR_CONFIG = new InjectionToken<NavbarConfig>('NAVBAR_CONFIG');
