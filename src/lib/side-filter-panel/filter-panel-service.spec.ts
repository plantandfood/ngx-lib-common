import { async, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FilterContext } from './filter-context';
import { FilterPanelService } from './filter-panel-service';

interface TestFilterModel {
  name: string;
  description: string;
}

fdescribe('FilterPanelService', () => {
  let service: FilterPanelService;
  let context: FilterContext<TestFilterModel>;

  const filters: FormlyFieldConfig[] = [
    { name: 'name' },
    { name: 'description' }
  ];

  const model: TestFilterModel = {
    name: 'Smiggles',
    description: "Man's best friend"
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      schemas: [],
      providers: [
        { provide: Router, useValue: {} },
        { provide: ActivatedRoute, useValue: {} }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.get(FilterPanelService);
    context = service.createContext(filters, model);
  });

  it('should create an instance', () => {
    expect(service).toBeTruthy();
  });

  it('should create of a new context instance', () => {
    expect(context).toBeTruthy();
  });

  it('should set hasContext state to true if context is in focus', () => {
    context.focus();
    expect(service.hasContext).toBeTruthy();
  });

  it('should set hasContext state to false if context is out of focus', () => {
    context.focus();
    context.unfocus();
    expect(service.hasContext).toBeFalsy();
  });

  it('should set the open state when isOpen = <boolean>true', () => {
    service.isOpen = true;
    expect(service.isOpen).toBe(true);
  });

  it('should set the open state to false when isOpen = <string>"true"', () => {
    service.isOpen = <any>'true';
    expect(service.isOpen).toBe(false);
  });

  it('should be visible if the opened state is true and active context is focused', () => {
    service.isOpen = true;
    context.focus();
    expect(service.isVisible).toBe(true);
  });

  describe('FilterContext', () => {
    it('should store the same model as used to create the context', () => {
      expect(context.model).toBe(model);
    });

    it('should provide a modelChanged$ observable, so we can subscribe to model changes', () => {
      let changedModel: any;
      context.modelChanged$.subscribe(value => (changedModel = value));
      expect(changedModel).toBe(model);
    });

    it('should provide a way to clear the filters and model of the context', () => {
      context.clearFilters();
      expect(context.model).toEqual(<TestFilterModel>{});
    });
  });
});
