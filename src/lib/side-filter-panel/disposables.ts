import { SubscriptionLike, Observable } from 'rxjs';

/**
 * Manages disposable resources such as rxjs Subscriptions.
 */
export class Disposables {
  private _subscriptions: SubscriptionLike[];

  addSubscription(subscription: SubscriptionLike) {
    if (!this._subscriptions) this._subscriptions = [];
    this._subscriptions.push(subscription);
  }

  /**
   * Subscribes to an observable.
   */
  subscribeTo<T>(
    observable: Observable<T>,
    next?: (value: T) => void,
    error?: (error: any) => void,
    complete?: () => void
  ) {
    this.addSubscription(observable.subscribe(next, error, complete));
  }

  /**
   * Releases all resources maintained by this instance.
   */
  dispose() {
    if (!this._subscriptions) return;
    this._subscriptions.forEach(s => {
      s.unsubscribe();
    });
    this._subscriptions = undefined;
  }
}
