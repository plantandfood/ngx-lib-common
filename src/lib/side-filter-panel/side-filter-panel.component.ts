import { Component, OnDestroy, Input } from '@angular/core';
import { ViewFilterContext } from './view-filter-context';
import { FormGroup } from '@angular/forms';
import { FilterPanelService } from './filter-panel-service';
import { FilterPanelController } from './filter-panel-controller';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Disposables } from './disposables';

@Component({
  selector: 'lib-side-filter-panel',
  templateUrl: './side-filter-panel.component.html',
  styleUrls: ['./side-filter-panel.component.scss']
})
export class SideFilterPanelComponent implements OnDestroy {
  private _disposables: Disposables = new Disposables();
  private _currentContext: ViewFilterContext;
  private _hasFilter: boolean = false;
  private _isBusy: boolean = false;
  private _isDisabled: boolean = false;
  private _modelChangeSuppressed: boolean = false;
  private _controller: FilterPanelController;

  @Input() isHandset: boolean;
  form: FormGroup;
  formModel: any;

  constructor(private service: FilterPanelService) {
    let that = this;
    this._controller = <FilterPanelController>{
      get hasFilter() {
        return that._hasFilter;
      },
      applyContext: (context: ViewFilterContext) => {
        // Handle any context-changed state changes (reconstruct the formly form, etc)
        that.formModel = context.model;
        that.form = new FormGroup({});
        that._currentContext = context;
        that.checkFilterState();
      },
      applyBusyState: (isBusy: boolean) => {
        that._isBusy = isBusy === true;
      },
      applyDisabledState: (disabled: boolean) => {
        that._suppressModelChange(() => {
          that._isDisabled = disabled === true;
          if (that._isDisabled) {
            that.form.disable({ emitEvent: false });
          } else {
            that.form.enable({ emitEvent: false });
          }
        });
      },
      clearFilters: () => {
        that.clearFilters();
      },
      resynch: () => {
        that._suppressModelChange(() => {
          that.form.patchValue(that.formModel);
          that.checkFilterState();
        });
        that._currentContext.raiseModelChanged();
      }
    };

    (<any>service).__bindFilterPanel(this._controller);
  }

  /**
   * Suppress model change events raised by Formly. In some cases, changing the form state causes Formly to raise a modelChange event.
   * We don't want to propagate it to the current client context (as the model itself has not changed) so we set the _modelChangeSuppressed flag to ignore it.
   * @param operation operation you wish to perform wrapped by model suppression
   */
  private _suppressModelChange(operation: () => void): void {
    this._modelChangeSuppressed = true;
    try {
      operation();
    } finally {
      this._modelChangeSuppressed = false;
    }
  }

  /**
   * Used internally by FilterPanelComponent.
   */
  get filters(): FormlyFieldConfig[] {
    return this._currentContext.formlyConfig;
  }

  /**
   * Used internally by FilterPanelComponent.
   */
  get isBusy(): boolean {
    return this._isBusy;
  }

  /**
   * Used internally by FilterPanelComponent.
   */
  get isDisabled(): boolean {
    return this._isDisabled;
  }

  /**
   * Used internally by FilterPanelComponent.
   */
  get opened(): boolean {
    return !this._currentContext.isEmpty && this.service.isOpen === true;
  }

  ngOnDestroy() {
    this._disposables.dispose();
    (<any>this.service).__unbindFilterPanel(this._controller);
  }

  /**
   * Used internally by FilterPanelComponent.
   */
  onModelChange() {
    // When the formly filter-model is in the proper state, raise the model-changed event consumed by the current client context.
    if (this._modelChangeSuppressed) return;
    this.checkFilterState();
    this._currentContext.raiseModelChanged();
  }

  /**
   * Used internally by FilterPanelComponent.
   */
  closeClick() {
    this.service.isOpen = false;
  }

  /**
   * Used internally by FilterPanelComponent.
   */
  clearFilters() {
    if (!hasValue(this._currentContext.model)) return;

    this._suppressModelChange(() => {
      this.form.reset({ emitEvent: false });
      this.checkFilterState();
    });

    this._currentContext.raiseModelChanged();
  }

  private checkFilterState() {
    this._hasFilter = hasValue(this._currentContext.model);
  }
}

/**
 * Recursively checks the properties of a model for assigned values.
 */
function hasValue(model: any): boolean {
  if (model == undefined) return false;
  if (typeof model === 'string' && model !== '') return true;
  let result = false;
  if (Array.isArray(model)) {
    for (let i = 0; !result && i < model.length; i++) {
      result = hasValue(model[i]);
    }
  } else {
    let keys = Object.keys(model);
    for (let i = 0; !result && i < keys.length; i++) {
      result = hasValue(model[keys[i]]);
    }
  }

  return result;
}
