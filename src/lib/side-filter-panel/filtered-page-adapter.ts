import { Params } from '@angular/router';
import { PageEvent } from '@angular/material';

/** Allows a FilteredQueryCoordinator to adapt to client-specfic paging query parameters. */
export interface FilterPageAdapter {
  /**
   * Applies paging data from an  @angular/material PageEvent to local query parameters.
   * @param params The parameter collection to apply the paging data to.
   * @param page The material page event.
   */
  applyState(params: Params, page: PageEvent);
  /**
   * Resets the local query parameters to specify the default (initial) page.
   * @param params The parameter collection to apply the paging data to.
   */
  resetPageIndex(params: Params);
}
