import { ViewFilterContext } from './view-filter-context';

export interface FilterPanelController {
  readonly hasFilter: boolean;
  applyContext(context: ViewFilterContext): void;
  applyBusyState(isDisabled: boolean): void;
  applyDisabledState(isDisabled: boolean): void;
  clearFilters(): void;
  resynch(): void;
}
