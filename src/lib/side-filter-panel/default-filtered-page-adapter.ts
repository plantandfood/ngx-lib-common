import { Params } from '@angular/router';
import { PageEvent } from '@angular/material';
import { FilterPageAdapter } from './filtered-page-adapter';

export class DefaultFilteredPageAdapter implements FilterPageAdapter {
  applyState(params: Params, page: PageEvent) {
    params.pageIndex = page.pageIndex;
    params.pageSize = page.pageSize;
  }

  resetPageIndex(params: Params) {
    delete params.pageIndex;
  }
}
