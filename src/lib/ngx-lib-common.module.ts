import { NgModule } from '@angular/core';
import { SideNavbarComponent } from './side-navbar/side-navbar.component';
import { NgxLibMaterialModule } from 'ngx-lib-material';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { SideFilterPanelComponent } from './side-filter-panel/side-filter-panel.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormlyMaterialModule } from '@ngx-formly/material';

@NgModule({
  declarations: [SideNavbarComponent, TopNavbarComponent, SideFilterPanelComponent],
  imports: [
    CommonModule,
    RouterModule,
    NgSelectModule,
    NgxLibMaterialModule,
    ReactiveFormsModule,
    FormlyMaterialModule,
    FormlyModule.forRoot(),
  ],
  exports: [RouterModule, SideNavbarComponent, TopNavbarComponent, SideFilterPanelComponent]
})
export class NgxLibCommonModule { }
